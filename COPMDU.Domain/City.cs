﻿using Dapper.Contrib.Extensions;
using System.Collections.Generic;

namespace COPMDU.Domain
{
    public class City : Base
    {
        public bool EnabledInteration { get; set; }

        public int ExternalSystemId { get; set; }

        public int OperatorId { get; set; }

        public ExternalSystem ExternalSystem { get; set; }
    }
}