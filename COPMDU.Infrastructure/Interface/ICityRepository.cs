﻿using COPMDU.Domain;
using System.Collections.Generic;

namespace COPMDU.Infrastructure.Interface
{
    public interface ICityRepository
    {
        City Get(string name);
    }
}